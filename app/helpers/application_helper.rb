module ApplicationHelper
  
  # ページごとの完全なタイトルを返します。
  # titleがない場合、Ruby on Rails Tutorial…と表示します。
  def full_title(page_title = '')
    base_title = "Ruby on Rails Tutorial Sample App"
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title
    end
  end
end
